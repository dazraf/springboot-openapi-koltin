package com.example

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@SpringBootApplication
class DemoApplication

@Component
class StartupApplicationListener : ApplicationListener<ContextRefreshedEvent?> {

	override fun onApplicationEvent(event: ContextRefreshedEvent?) {
		log.info("application started")
		log.info("swagger - http://localhost:8080/swagger-ui.html")
	}

	companion object {
		val log = LoggerFactory.getLogger(StartupApplicationListener::class.java)
	}
}

@RestController
class GreetingController {

	private val counter = AtomicLong()
	@GetMapping("/greeting")
	fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String?): Greeting {
		return Greeting(counter.incrementAndGet(), String.format(template, name))
	}

	companion object {
		private const val template = "Hello, %s!"
	}
}

data class Greeting(val id: Long, val content: String)

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args)
}
